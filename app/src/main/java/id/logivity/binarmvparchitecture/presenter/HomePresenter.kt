package id.logivity.binarmvparchitecture.presenter

import id.logivity.binarmvparchitecture.model.Student

class HomePresenter(private val callback: Callback) {

    fun getStudentList(jurusan: String) {
        var studentList = dummyDataFromServer()

        if (jurusan != "Semua") {
            studentList = studentList.filter {
                it.jurusan == jurusan
            }.toMutableList()
        }

        if (studentList.isEmpty()) {
            callback.onGetStudentFailure("Tidak ada murid")
        } else {
            callback.onGetStudentSuccess(studentList)
        }
    }

    private fun dummyDataFromServer(): MutableList<Student> {
        val studentList = mutableListOf<Student>()
        studentList.add(Student("15.11.8570", "Dhiya Ulhaq Zulha Alamsyah", "Informatika", "IF 02"))
        studentList.add(Student("15.11.8571", "Khamdan Nahari", "Informatika", "IF 02"))
        studentList.add(Student("15.11.8572", "Laode Darlansyah", "Sistem Informasi", "SI 01"))
        studentList.add(Student("15.11.8573", "Abika Chairul Yusri", "Sistem Informasi", "SI 01"))
        studentList.add(Student("15.11.8574", "Flabi Raya", "Teknik Komputer", "TK 03"))
        studentList.add(Student("15.11.8575", "Abdul Ghafar", "Teknik Komputer", "TK 03"))

        return studentList
    }

    interface Callback {
        fun onGetStudentSuccess(studentList: MutableList<Student>)
        fun onGetStudentFailure(errMsg: String)
    }
}