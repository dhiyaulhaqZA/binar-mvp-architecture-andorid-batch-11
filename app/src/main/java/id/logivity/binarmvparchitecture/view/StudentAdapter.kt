package id.logivity.binarmvparchitecture.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.logivity.binarmvparchitecture.R
import id.logivity.binarmvparchitecture.model.Student
import kotlinx.android.synthetic.main.item_student.view.*

class StudentAdapter(private val callback: Callback): RecyclerView.Adapter<StudentAdapter.ViewHolder>() {

    private lateinit var ctx: Context
    private val studentList = mutableListOf<Student>()

    fun addStudentList(students: MutableList<Student>) {
        studentList.clear()
        studentList.addAll(students)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        ctx = parent.context
        val view = LayoutInflater.from(ctx).inflate(R.layout.item_student, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return studentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }


    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                callback.onStudentItemClick(studentList[adapterPosition])
            }
        }
        fun bind(position: Int) {
            val student = studentList[position]
            itemView.tv_item_student_name.text = student.nama
            itemView.tv_item_student_jurusan.text = student.jurusan
        }
    }

    interface Callback {
        fun onStudentItemClick(student: Student)
    }
}