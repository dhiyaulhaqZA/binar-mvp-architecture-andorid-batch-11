package id.logivity.binarmvparchitecture.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import id.logivity.binarmvparchitecture.R
import id.logivity.binarmvparchitecture.model.Student
import id.logivity.binarmvparchitecture.presenter.HomePresenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), HomePresenter.Callback, StudentAdapter.Callback {

    private lateinit var homePresenter: HomePresenter
    private lateinit var studentAdapter: StudentAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupComponent()

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val jurusan = when (item?.itemId) {
            R.id.action_filter_informatika -> "Informatika"
            R.id.action_filter_sistem_informasi -> "Sistem Informasi"
            R.id.action_filter_teknik_komputer -> "Teknik Komputer"
            R.id.action_filter_ilmu_komunikasi -> "Ilmu Komunikasi"
            R.id.action_filter_semua -> "Semua"
            else -> "Semua"
        }
        homePresenter.getStudentList(jurusan)
        return super.onOptionsItemSelected(item)
    }

    private fun setupComponent() {
        studentAdapter = StudentAdapter(this)
        rv_main.setHasFixedSize(true)
        rv_main.layoutManager = LinearLayoutManager(this)
        rv_main.adapter = studentAdapter

        homePresenter = HomePresenter(this)
        homePresenter.getStudentList("Semua")
    }

    override fun onGetStudentSuccess(studentList: MutableList<Student>) {
        studentAdapter.addStudentList(studentList)
    }

    override fun onGetStudentFailure(errMsg: String) {
        Toast.makeText(this, errMsg, Toast.LENGTH_SHORT).show()
    }

    override fun onStudentItemClick(student: Student) {
        Toast.makeText(this, student.nama, Toast.LENGTH_SHORT).show()
    }
}
