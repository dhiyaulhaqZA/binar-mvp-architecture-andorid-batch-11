package id.logivity.binarmvparchitecture.model

data class Student(val nim: String,
                   val nama: String,
                   val jurusan: String,
                   val kelas: String)